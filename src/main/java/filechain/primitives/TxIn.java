/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;

import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

/**
 * An input of a transaction. It contains the location of the previous
 * transaction's output that it claims and a signature that matches the output's
 * public key.
 */
public class TxIn {

	/**
	 * Setting nSequence to this value for every input in a transaction disables
	 * nLockTime.
	 */
	public static final int SEQUENCE_FINAL = 0xFFFFFFFF;

	public OutPoint prevout = new OutPoint();
	public Script scriptSig = new Script();
	public int nSequence;

	public TxIn() {
		setNull();
	}

	public TxIn(OutPoint prevoutIn, Script scriptSigIn, int nSequenceIn) {
		prevout = prevoutIn;
		scriptSig = scriptSigIn;
		nSequence = nSequenceIn;
	}

	public TxIn(Hash256 hashPrevTx, int nOut, Script scriptSigIn, int nSequenceIn) {
		prevout = new OutPoint(hashPrevTx, nOut);
		scriptSig = scriptSigIn;
		nSequence = nSequenceIn;
	}

	public void setNull() {
		prevout.setNull();
		scriptSig.setNull();
		nSequence = SEQUENCE_FINAL;
	}

	public boolean isNull() {
		return (scriptSig.isNull() && nSequence == SEQUENCE_FINAL);
	}

	public boolean equals(TxIn other) {
		return (prevout.equals(other.prevout) && scriptSig.equals(other.scriptSig) && nSequence == other.nSequence);
	}

	public boolean notEquals(TxIn other) {
		return !equals(other);
	}

	public void unserialize(Stream s) throws IOException {
		prevout = s.read(OutPoint.class);
		scriptSig = s.read(Script.class);
		nSequence = s.read(Integer.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(prevout);
		s.write(scriptSig);
		s.write(nSequence);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(String.format("TxIn(prevout=%s", prevout.toString()));
		if (prevout.isNull())
			str.append(String.format(", coinbase %s", scriptSig.toString()));
		else
			str.append(String.format(", scriptSig %s", scriptSig.toString()));
		if (nSequence != SEQUENCE_FINAL)
			str.append(String.format(", nSequence %d", nSequence));
		str.append(")");
		return str.toString();
	}

}
