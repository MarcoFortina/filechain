/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;
import java.util.ArrayList;

import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

public class Block {

	// network and disk
	public BlockHeader header = new BlockHeader();
	public ArrayList<Transaction> vtx = new ArrayList<Transaction>();

	// memory only
	public boolean fChecked;

	public Block() {
		setNull();
	}

	public void setNull() {
		header.setNull();
		vtx.clear();
		fChecked = false;
	}

	public void unserialize(Stream s) throws IOException {
		header.unserialize(s);
		vtx = s.readArray(Transaction.class);
	}

	public void serialize(Stream s) throws IOException {
		header.serialize(s);
		s.writeArray(vtx);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		Hash256 hash = null;
		try {
			hash = header.getHash();
		} catch (IOException e) {
			e.printStackTrace();
		}
		str.append(String.format(
				"Block(hash=%s, ver=%d, hashPrevBlock=%s, hashMerkleRoot=%s, nTime=%d, nBits=%d, nNonce=%d, vtx.size=%d)\n",
				hash.toString(), header.nVersion, header.hashPrevBlock.toString(), header.hashMerkleRoot.toString(),
				header.nTime, header.nBits, header.nNonce, vtx.size()));
		for (int i = 0; i < vtx.size(); i++) {
			str.append("  " + vtx.get(i).toString() + "\n");
		}
		return str.toString();
	}

	public void buildMerkleRoot() throws IOException {
		MerkleTree merkleTree = new MerkleTree();
		header.hashMerkleRoot = merkleTree.blockMerkleRoot(this);
	}

}
