/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;
import java.util.ArrayList;

import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

/**
 * Describes a place in the block chain to another node such that if the other
 * node doesn't have the same branch, it can find a recent common trunk. The
 * further back it is, the further before the fork it may be.
 */
public class BlockLocator {

	ArrayList<Hash256> vHave = new ArrayList<Hash256>();

	public BlockLocator() {
		setNull();
	}

	public BlockLocator(ArrayList<Hash256> have) {
		vHave = have;
	}

	public void unserialize(Stream s) throws IOException {
		vHave = s.readArray(Hash256.class);
	}

	public void serialize(Stream s) throws IOException {
		s.writeArray(vHave);
	}

	void setNull() {
		vHave.clear();
	}

	public boolean isNull() {
		return vHave.isEmpty();
	}

}
