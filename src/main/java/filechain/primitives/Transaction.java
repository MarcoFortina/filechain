/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;
import java.util.ArrayList;

import filechain.crypto.Sha256;
import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

/**
 * The basic transaction that is broadcasted on the network and contained in
 * blocks. A transaction can contain multiple inputs and outputs.
 */
public class Transaction {

	/**
	 * Default transaction version.
	 */
	public static final int CURRENT_VERSION = 1;

	public int nVersion;
	public ArrayList<TxIn> vin = new ArrayList<TxIn>();
	public ArrayList<TxOut> vout = new ArrayList<TxOut>();
	public int nLockTime;

	public Transaction() {
		setNull();
	}

	public void setNull() {
		nVersion = CURRENT_VERSION;
		vin.clear();
		vout.clear();
		nLockTime = 0;
	}

	public boolean isNull() {
		return vin.isEmpty() && vout.isEmpty();
	}

	public boolean isCoinBase() {
		return (vin.size() == 1 && vin.get(0).prevout.isNull());
	}

	public boolean equals(Transaction other) throws IOException {
		return (getHash().equals(other.getHash()));
	}

	public boolean notEquals(Transaction other) throws IOException {
		return !equals(other);
	}

	public void unserialize(Stream s) throws IOException {
		nVersion = s.read(Integer.class);
		vin = s.readArray(TxIn.class);
		vout = s.readArray(TxOut.class);
		nLockTime = s.read(Integer.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(nVersion);
		s.writeArray(vin);
		s.writeArray(vout);
		s.write(nLockTime);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	public Hash256 getHash() throws IOException {
		Stream s = new Stream();
		serialize(s);

		Sha256 sha256 = new Sha256();
		return sha256.hash(s.data());
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		Hash256 hash = null;
		try {
			hash = getHash();
		} catch (IOException e) {
			e.printStackTrace();
		}
		str.append(String.format("Transaction(hash=%s, ver=%d, vin.size=%d, vout.size=%d, nLockTime=%d)\n",
				hash.toString(), nVersion, vin.size(), vout.size(), nLockTime));
		for (int i = 0; i < vin.size(); i++) {
			str.append("    " + vin.get(i).toString() + "\n");
		}
		for (int i = 0; i < vout.size(); i++) {
			str.append("    " + vout.get(i).toString() + "\n");
		}
		return str.toString();
	}

}
