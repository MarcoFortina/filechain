/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;

import filechain.crypto.Sha256;
import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

/**
 * Nodes collect new transactions into a block, hash them into a hash tree, and
 * scan through nonce values to make the block's hash satisfy proof-of-work
 * requirements. When they solve the proof-of-work, they broadcast the block to
 * everyone and the block is added to the block chain. The first transaction in
 * the block is a special one that creates a new coin owned by the creator of
 * the block.
 */
public class BlockHeader {

	public static final int CURRENT_VERSION = 1;

	// header
	public int nVersion;
	public Hash256 hashPrevBlock = new Hash256();
	public Hash256 hashMerkleRoot = new Hash256();
	public int nTime;
	public int nBits;
	public int nNonce;

	public BlockHeader() {
		setNull();
	}

	public void setNull() {
		nVersion = BlockHeader.CURRENT_VERSION;
		hashPrevBlock.setNull();
		hashMerkleRoot.setNull();
		nTime = 0;
		nBits = 0;
		nNonce = 0;
	}

	public boolean isNull() {
		return (nBits == 0);
	}

	public void unserialize(Stream s) throws IOException {
		nVersion = s.read(Integer.class);
		hashPrevBlock = s.read(Hash256.class);
		hashMerkleRoot = s.read(Hash256.class);
		nTime = s.read(Integer.class);
		nBits = s.read(Integer.class);
		nNonce = s.read(Integer.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(nVersion);
		s.write(hashPrevBlock);
		s.write(hashMerkleRoot);
		s.write(nTime);
		s.write(nBits);
		s.write(nNonce);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	public Hash256 getHash() throws IOException {
		Stream s = new Stream();
		serialize(s);

		Sha256 sha256 = new Sha256();
		return sha256.hash(s.data());
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		Hash256 hash = null;
		try {
			hash = getHash();
		} catch (IOException e) {
			e.printStackTrace();
		}
		str.append(String.format(
				"BlockHeader(hash=%s, ver=%d, hashPrevBlock=%s, hashMerkleRoot=%s, nTime=%d, nBits=%d, nNonce=%d)\n",
				hash.toString(), nVersion, hashPrevBlock.toString(), hashMerkleRoot.toString(), nTime, nBits, nNonce));
		return str.toString();
	}

}
