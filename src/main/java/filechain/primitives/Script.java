/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;
import java.util.Arrays;

import filechain.serialize.Stream;

public class Script {

	// TODO
	byte[] asm = new byte[1024];

	public Script() {
		setNull();
	}

	public void setNull() {
		Arrays.fill(asm, (byte) 0);
	}

	public boolean isNull() {
		for (int i = 0; i < 32; i++)
			if (asm[i] != 0)
				return false;
		return true;
	}

	public void unserialize(Stream s) throws IOException {
		asm = s.read(byte[].class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(asm);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	@Override
	public String toString() {
		// TODO
		return null;
	}

}
