/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;

import filechain.datatypes.Hash256;
import filechain.serialize.Stream;

/**
 * An outpoint - a combination of a transaction hash and an index n into its
 * vout
 */
public class OutPoint {

	private static int NULL_INDEX = 0xFFFFFFFF;

	public Hash256 hash = new Hash256();
	public int n;

	public OutPoint() {
		setNull();
	}

	public OutPoint(Hash256 hashIn, int nIn) {
		hash = hashIn;
		n = nIn;
	}

	public void setNull() {
		hash.setNull();
		n = NULL_INDEX;
	}

	public boolean isNull() {
		return (hash.isNull() && n == NULL_INDEX);
	}

	public boolean equals(OutPoint other) {
		return ((hash.compareTo(other.hash) == 0) && n == other.n);
	}

	public boolean notEquals(OutPoint other) {
		return !equals(other);
	}

	public void unserialize(Stream s) throws IOException {
		hash = s.read(Hash256.class);
		n = s.read(Integer.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(hash);
		s.write(n);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	@Override
	public String toString() {
		return String.format("OutPoint(hash=%s, n=%d)", hash.toString(), n);
	}

}
