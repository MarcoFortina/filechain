/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;

import filechain.serialize.Stream;

/**
 * An output of a transaction. It contains the public key that the next input
 * must be able to sign with to claim it.
 */
public class TxOut {

	public long nValue;
	public Script scriptPubKey = new Script();

	public TxOut() {
		setNull();
	}

	public TxOut(int nValueIn, Script scriptPubKeyIn) {
		nValue = nValueIn;
		scriptPubKey = scriptPubKeyIn;
	}

	public void setNull() {
		nValue = -1;
		scriptPubKey.setNull();
	}

	public boolean isNull() {
		return (nValue == -1);
	}

	public boolean equals(TxOut other) {
		return (nValue == other.nValue && scriptPubKey.equals(other.scriptPubKey));
	}

	public boolean notEquals(TxOut other) {
		return !equals(other);
	}

	public void unserialize(Stream s) throws IOException {
		nValue = s.read(Long.class);
		scriptPubKey = s.read(Script.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(nValue);
		s.write(scriptPubKey);
	}

	public int size() throws IOException {
		Stream s = new Stream();
		serialize(s);
		return s.size();
	}

	@Override
	public String toString() {
		return String.format("TxOut(nValue=%d, scriptPubKey=%s)", nValue, scriptPubKey.toString());
	}

}
