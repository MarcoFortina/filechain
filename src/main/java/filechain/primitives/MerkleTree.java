/*
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.primitives;

import java.io.IOException;
import java.util.ArrayList;

import filechain.crypto.Sha256;
import filechain.datatypes.Hash256;

/*
 * WARNING! If you're reading this because you're learning about crypto
 * and/or designing a new system that will use merkle trees, keep in mind
 * that the following merkle tree algorithm has a serious flaw related to
 * duplicate txids, resulting in a vulnerability (CVE-2012-2459).
 * The reason is that if the number of hashes in the list at a given level
 * is odd, the last one is duplicated before computing the next level (which
 * is unusual in Merkle trees). This results in certain sequences of
 * transactions leading to the same merkle root. For example, these two
 * trees:
 *               A               A
 *             /  \            /   \
 *           B     C         B       C
 *          / \    |        / \     / \
 *         D   E   F       D   E   F   F
 *        / \ / \ / \     / \ / \ / \ / \
 *        1 2 3 4 5 6     1 2 3 4 5 6 5 6
 * for transaction lists [1,2,3,4,5,6] and [1,2,3,4,5,6,5,6] (where 5 and
 * 6 are repeated) result in the same root hash A (because the hash of both
 * of (F) and (F,F) is C).
 * The vulnerability results from being able to send a block with such a
 * transaction list, with the same merkle root, and the same block hash as
 * the original without duplication, resulting in failed validation. If the
 * receiving node proceeds to mark that block as permanently invalid
 * however, it will fail to accept further unmodified (and thus potentially
 * valid) versions of the same block. We defend against this by detecting
 * the case where we would hash two identical hashes at the end of the list
 * together, and treating that identically to the block having an invalid
 * merkle root. Assuming no double-SHA256 collisions, this will detect all
 * known ways of changing the transactions without affecting the merkle
 * root.
 */
public class MerkleTree {

	private ArrayList<Hash256> sha256d64(ArrayList<Hash256> hashes) throws IOException {
		ArrayList<Hash256> tmp = new ArrayList<Hash256>();

		for (int i = 0; i < hashes.size(); i += 2) {
			byte[] left = hashes.get(i).data();
			byte[] right = hashes.get(i + 1).data();

			byte[] c1 = new byte[left.length + right.length];

			System.arraycopy(left, 0, c1, 0, left.length);
			System.arraycopy(right, 0, c1, left.length, right.length);

			Sha256 sha256 = new Sha256();
			Hash256 hashed = sha256.doubleHash(c1);

			tmp.add(hashed);
		}

		return tmp;
	}

	private Hash256 computeMerkleRoot(ArrayList<Hash256> hashes) throws IOException {
		ArrayList<Hash256> tmp = new ArrayList<Hash256>();

		for (int i = 0; i < hashes.size(); i++) {
			tmp.add(hashes.get(i));
		}

		while (tmp.size() > 1) {
			if (tmp.size() % 2 != 0) {
				tmp.add(tmp.get(tmp.size() - 1));
			}
			tmp = sha256d64(tmp);
		}

		if (tmp.size() == 0)
			return new Hash256();

		byte[] m_data = tmp.get(0).data();
		int length = m_data.length;
		byte[] m_data_rev = new byte[length];

		for (int i = 0; i < length; ++i) {
			m_data_rev[i] = m_data[(length - 1) - i];
		}

		Hash256 root = new Hash256(m_data_rev);

		return root;
	}

	/*
	 * Compute the Merkle root of the transactions in a block.
	 */
	public Hash256 blockMerkleRoot(Block block) throws IOException {
		ArrayList<Hash256> leaves = new ArrayList<Hash256>();

		for (int s = 0; s < block.vtx.size(); s++) {
			leaves.add(block.vtx.get(s).getHash());
		}

		return computeMerkleRoot(leaves);
	}

}
