/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.datatypes;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

import filechain.utils.Utils;

public class Hash256 {

	private byte[] bytes = new byte[32];

	public Hash256() {
		setNull();
	}

	public Hash256(byte[] bytesIn) throws IOException {
		set(bytesIn);
	}

	public void setNull() {
		Arrays.fill(bytes, (byte) 0);
	}

	public boolean isNull() {
		for (int i = 0; i < 32; i++)
			if (bytes[i] != 0)
				return false;
		return true;
	}

	public byte[] data() {
		return bytes;
	}

	public int compareTo(Hash256 other) {
		BigInteger big1 = new BigInteger(this.data());
		BigInteger big2 = new BigInteger(other.data());
		return big1.compareTo(big2);
	}

	public boolean equals(Hash256 other) {
		return (compareTo(other) == 0);
	}

	public void set(byte[] data) throws IOException {
		if (data.length != 32)
			throw new IOException();
		bytes = data;
	}

	public void set(int n) throws IOException {
		byte[] bytes = ByteBuffer.allocate(4).putInt(n).array();
		set(Arrays.copyOf(bytes, 32));
	}

	@Override
	public String toString() {
		return Utils.hexStr(bytes);
	}

}
