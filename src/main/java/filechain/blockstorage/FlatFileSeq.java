/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.blockstorage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FlatFileSeq represents a sequence of numbered files storing raw data. This
 * class facilitates access to and efficient management of these files.
 */
public class FlatFileSeq {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlatFileSeq.class);

	private Path m_dir;
	private String m_prefix;
	private int m_chunk_size;

	public FlatFileSeq(Path dir, String prefix, int chunk_size) throws Exception {
		m_dir = dir;
		m_prefix = prefix;
		m_chunk_size = chunk_size;

		if (chunk_size == 0) {
			throw new Exception("chunk_size must be positive");
		}
	}

	/**
	 * Get the name of the file at the given position.
	 */
	public Path fileName(FlatFilePos pos) {
		return Paths.get(m_dir.toString() + "/" + String.format("%s%05d.dat", m_prefix, pos.nFile));

	};

	/**
	 * Open a handle to the file at the given position.
	 */
	public RandomAccessFile open(FlatFilePos pos, boolean read_only) throws IOException {
		if (pos.isNull()) {
			return null;
		}

		Path path = fileName(pos);
		FS.createDirectories(path.getParent());
		RandomAccessFile file;

		try {
			if (read_only)
				file = new RandomAccessFile(path.toFile(), "r");
			else
				file = new RandomAccessFile(path.toFile(), "rws");
		} catch (FileNotFoundException e) {
			LOGGER.error("Unable to open file %s\n", path.toString());
			return null;
		}

		try {
			file.seek(pos.nPos);
		} catch (IOException e) {
			LOGGER.error("Unable to seek to position %d of %s\n", pos.nPos, path.toString());
			file.close();
			return null;
		}

		return file;
	}

	/**
	 * Allocate additional space in a file after the given starting position. The
	 * amount allocated will be the minimum multiple of the sequence chunk size
	 * greater than add_size.
	 */
	public int allocate(FlatFilePos pos, int add_size) throws IOException {
		int n_old_chunks = (pos.nPos + m_chunk_size - 1) / m_chunk_size;
		int n_new_chunks = (pos.nPos + add_size + m_chunk_size - 1) / m_chunk_size;
		if (n_new_chunks > n_old_chunks) {
			int old_size = pos.nPos;
			int new_size = n_new_chunks * m_chunk_size;
			int inc_size = new_size - old_size;

			if (FS.checkDiskSpace(m_dir, inc_size)) {
				RandomAccessFile file = open(pos, false);
				if (file != null) {
					LOGGER.info("Pre-allocating up to position 0x%x in %s%05u.dat\n", new_size, m_prefix, pos.nFile);
					FS.allocateFileRange(file, pos.nPos, inc_size);
					file.close();
					return inc_size;
				}
			} else {
				return -1;
			}
		}
		return 0;
	}

	/**
	 * Commit a file to disk, and optionally truncate off extra pre-allocated bytes
	 * if final.
	 */
	public boolean flush(FlatFilePos pos, boolean finalize) throws IOException {
		RandomAccessFile file = open(new FlatFilePos(pos.nFile, 0), false); // Avoid fseek to nPos
		if (file == null) {
			LOGGER.error("Failed to open file %d", pos.nFile);
			return false;
		}
		if (finalize && !FS.truncateFile(file, pos.nPos)) {
			file.close();
			LOGGER.error("Failed to truncate file %d", pos.nFile);
			return false;
		}
		if (!FS.fileCommit(file)) {
			file.close();
			LOGGER.error("Failed to commit file %d", pos.nFile);
			return false;
		}
		FS.directoryCommit(m_dir);

		file.close();

		return true;
	}

}
