/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.blockstorage;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FS {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlatFileSeq.class);

	/**
	 * Create directory (and if necessary its parents), unless the leaf directory
	 * already exists or is a symlink to an existing directory.
	 */
	static boolean createDirectories(Path p) throws IOException {

		if (Files.isSymbolicLink(p) && Files.isDirectory(p)) {
			return false;
		}
		return Files.createDirectories(p) != null;
	}

	static boolean checkDiskSpace(Path dir, long additional_bytes) throws IOException {
		long min_disk_space = 52428800; // 50 MiB

		FileStore store = Files.getFileStore(dir);
		long free_bytes_available = store.getTotalSpace();
		return free_bytes_available >= min_disk_space + additional_bytes;
	}

	/**
	 * this function tries to make a particular range of a file allocated
	 * (corresponding to disk space) it is advisory, and the range specified in the
	 * arguments will never contain live data
	 */
	static void allocateFileRange(RandomAccessFile file, int offset, int length) throws IOException {
		int nEndPos = offset + length;

		if (nEndPos <= file.length())
			return;

		file.setLength(nEndPos);
	}

	static boolean truncateFile(RandomAccessFile file, int length) {
		try {
			file.setLength(length);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	static boolean fileCommit(RandomAccessFile file) {
		try {
			file.getFD().sync();
		} catch (IOException e) {
			LOGGER.error("Sync failed: %s\n", e.getMessage());
			return false;
		}

		return true;
	}

	static void directoryCommit(Path dirname) throws IOException {
		RandomAccessFile file = new RandomAccessFile(dirname.toFile(), "r");
		if (file != null) {
			file.getFD().sync();
			file.close();
		}
	}

}
