/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.blockstorage;

import java.io.IOException;

import filechain.serialize.Stream;

public class BlockFileInfo {

	/**
	 * number of blocks stored in file
	 */
	public int nBlocks;

	/**
	 * number of used bytes of block file
	 */
	public int nSize;

	/**
	 * number of used bytes in the undo file
	 */
	public int nUndoSize;

	/**
	 * lowest height of block in file
	 */
	public int nHeightFirst;

	/**
	 * highest height of block in file
	 */
	public int nHeightLast;

	/**
	 * earliest time of block in file
	 */
	public long nTimeFirst;

	/**
	 * latest time of block in file
	 */
	public long nTimeLast;

	public BlockFileInfo() {
		setNull();
	}

	public void setNull() {
		nBlocks = 0;
		nSize = 0;
		nUndoSize = 0;
		nHeightFirst = 0;
		nHeightLast = 0;
		nTimeFirst = 0;
		nTimeLast = 0;
	}

	public void unserialize(Stream s) throws IOException {
		nBlocks = s.read(Integer.class);
		nSize = s.read(Integer.class);
		nUndoSize = s.read(Integer.class);
		nHeightFirst = s.read(Integer.class);
		nHeightLast = s.read(Integer.class);
		nTimeFirst = s.read(Long.class);
		nTimeLast = s.read(Long.class);
	}

	public void serialize(Stream s) throws IOException {
		s.write(nBlocks);
		s.write(nSize);
		s.write(nUndoSize);
		s.write(nHeightFirst);
		s.write(nHeightLast);
		s.write(nTimeFirst);
		s.write(nTimeLast);
	}

	// update statistics (does not update nSize)
	public void addBlock(int nHeightIn, long nTimeIn) {
		if (nBlocks == 0 || nHeightFirst > nHeightIn)
			nHeightFirst = nHeightIn;
		if (nBlocks == 0 || nTimeFirst > nTimeIn)
			nTimeFirst = nTimeIn;
		nBlocks++;
		if (nHeightIn > nHeightFirst)
			nHeightLast = nHeightIn;
		if (nTimeIn > nTimeLast)
			nTimeLast = nTimeIn;
	}

	@Override
	public String toString() {
		return String.format("BlockFileInfo(blocks=%d, size=%d, heights=%d...%d, time=%d...%d)", nBlocks, nSize,
				nHeightFirst, nHeightLast, nTimeFirst, nTimeLast);
	}

}
