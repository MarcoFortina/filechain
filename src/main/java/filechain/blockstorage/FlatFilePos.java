/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.blockstorage;

import java.io.IOException;

import filechain.serialize.Stream;

public class FlatFilePos {

	public int nFile;
	public int nPos;

	public FlatFilePos() {
		setNull();
	}

	public FlatFilePos(int nFileIn, int nPosIn) {
		nFile = nFileIn;
		nPos = nPosIn;
	}

	public boolean equals(FlatFilePos other) {
		return (nFile == other.nFile && nPos == other.nPos);
	}

	public boolean notEquals(FlatFilePos other) {
		return !equals(other);
	}

	public void setNull() {
		nFile = -1;
		nPos = 0;
	}

	public boolean isNull() {
		return (nFile == -1);
	}

	public void serialize(Stream ss) throws IOException {
		ss.write(nFile);
		ss.write(nPos);
	}

	public void unserialize(Stream ss) throws IOException {
		nFile = ss.read(nFile);
		nPos = ss.read(nPos);
	}

	@Override
	public String toString() {
		return String.format("FlatFilePos(nFile=%d, nPos=%d)", nFile, nPos);
	}

}
