/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.crypto;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import filechain.datatypes.Hash256;

public class Sha256 {

	public Hash256 hash(byte[] data) throws IOException {
		MessageDigest digest = null;
		byte[] encodedhash = null;

		try {
			digest = MessageDigest.getInstance("SHA-256");
			encodedhash = digest.digest(data);
		} catch (NoSuchAlgorithmException e) {
			// This should never happen
			e.printStackTrace();
		}

		return new Hash256(encodedhash);
	}

	public Hash256 doubleHash(byte[] data) throws IOException {
		Sha256 sha1 = new Sha256();
		Hash256 hash1 = sha1.hash(data);

		Sha256 sha2 = new Sha256();
		Hash256 hash2 = sha2.hash(hash1.data());

		return hash2;
	}

}
