/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HexFormat;

public class Utils {

	public static String hexStr(byte[] bytes) {
		return HexFormat.of().formatHex(bytes);
	}

	public static Path getBlocksDirPath() {
		return Paths.get(System.getProperty("user.home") + "/filechain/blocks");
	}

	public static boolean chekMask(int i) {
		return (i > 0);
	}

}
