/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain;

import filechain.blockstorage.FlatFilePos;
import filechain.datatypes.Hash256;
import filechain.primitives.BlockHeader;
import filechain.utils.Utils;

public class BlockIndex {

	// pointer to the hash of the block, if any. memory is owned by this BlockIndex
	public Hash256 phashBlock = new Hash256();

	// pointer to the index of the predecessor of this block
	public BlockIndex pprev;

	// (memory only) pointer to the index of the *active* successor of this block
	public BlockIndex pnext;

	// height of the entry in the chain. The genesis block has height 0
	public int nHeight;

	// Which # file this block is stored in (blk?????.dat)
	public int nFile;

	// Byte offset within blk?????.dat where this block's data is stored
	public int nDataPos;

	// Byte offset within rev?????.dat where this block's undo data is stored
	public int nUndoPos;

	// (memory only) Total amount of work (expected number of hashes) in the chain
	// up to and including this block
	public Hash256 nChainWork = new Hash256();

	// Number of transactions in this block.
	// Note: in a potential headers-first mode, this number cannot be relied upon
	public int nTx;

	// (memory only) Number of transactions in the chain up to and including this
	// block
	public int nChainTx; // change to 64-bit type when necessary; won't happen before 2030

	// Verification status of this block. See BlockStatus
	public int nStatus;

	// block header
	public int nVersion;
	Hash256 hashMerkleRoot = new Hash256();
	public int nTime;
	public int nBits;
	public int nNonce;

	public BlockIndex() {
		phashBlock.setNull();
		pprev = null;
		pnext = null;
		nHeight = 0;
		nFile = 0;
		nDataPos = 0;
		nUndoPos = 0;
		nChainWork.setNull();
		nTx = 0;
		nChainTx = 0;
		nStatus = 0;

		nVersion = 0;
		hashMerkleRoot.setNull();
		nTime = 0;
		nBits = 0;
		nNonce = 0;
	}

	public BlockIndex(BlockHeader block) {
		phashBlock.setNull();
		pprev = null;
		pnext = null;
		nHeight = 0;
		nFile = 0;
		nDataPos = 0;
		nUndoPos = 0;
		nChainWork.setNull();
		nTx = 0;
		nChainTx = 0;
		nStatus = 0;

		nVersion = block.nVersion;
		hashMerkleRoot = block.hashMerkleRoot;
		nTime = block.nTime;
		nBits = block.nBits;
		nNonce = block.nNonce;
	}

	public FlatFilePos getBlockPos() {
		FlatFilePos ret = new FlatFilePos();
		if (Utils.chekMask(nStatus & BlockStatus.BLOCK_HAVE_DATA)) {
			ret.nFile = nFile;
			ret.nPos = nDataPos;
		}
		return ret;
	}

	public FlatFilePos getUndoPos() {
		FlatFilePos ret = new FlatFilePos();
		if (Utils.chekMask(nStatus & BlockStatus.BLOCK_HAVE_UNDO)) {
			ret.nFile = nFile;
			ret.nPos = nUndoPos;
		}
		return ret;
	}

	public BlockHeader getBlockHeader() {
		BlockHeader block = new BlockHeader();
		block.nVersion = nVersion;
		if (pprev != null)
			block.hashPrevBlock = pprev.getBlockHash();
		block.hashMerkleRoot = hashMerkleRoot;
		block.nTime = nTime;
		block.nBits = nBits;
		block.nNonce = nNonce;
		return block;
	}

	public Hash256 getBlockHash() {
		return phashBlock;
	}

	public int getBlockTime() {
		return nTime;
	}

	public boolean checkIndex() {
		// TODO
		return true;
	}

	@Override
	public String toString() {
		return String.format("BlockIndex(pprev=%p, pnext=%p, nHeight=%d, merkle=%s, hashBlock=%s)", pprev, pnext,
				nHeight, hashMerkleRoot.toString(), getBlockHash().toString());
	}

}
