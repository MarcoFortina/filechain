/**
 * Copyright (c) [2022] Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain;

public class BlockStatus {

	public static int BLOCK_VALID_UNKNOWN = 0;

	// parsed, version ok, hash satisfies claimed PoW, 1 <= vtx count <= max,
	// timestamp not in future
	public static int BLOCK_VALID_HEADER = 1;

	// parent found, difficulty matches, timestamp >= median previous, checkpoint
	public static int BLOCK_VALID_TREE = 2;

	// only first tx is coinbase, 2 <= coinbase input script length <= 100,
	// transactions valid, no duplicate txids, sigops, size, merkle root
	public static int BLOCK_VALID_TRANSACTIONS = 3;

	// outputs do not overspend inputs, no double spends, coinbase output ok,
	// immature coinbase spends, BIP30
	public static int BLOCK_VALID_CHAIN = 4;

	// scripts/signatures ok
	public static int BLOCK_VALID_SCRIPTS = 5;

	// full block available in blk*.dat
	public static int BLOCK_HAVE_DATA = 8;

	// undo data available in rev*.dat
	public static int BLOCK_HAVE_UNDO = 16;

	// stage after last reached validness failed
	public static int BLOCK_FAILED_VALID = 32;

	// descends from failed block
	public static int BLOCK_FAILED_CHILD = 64;

	// mask
	public static int BLOCK_VALID_MASK = 7;
	public static int BLOCK_HAVE_MASK = 24;
	public static int BLOCK_FAILED_MASK = 96;

}
