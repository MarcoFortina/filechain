/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */
package filechain.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import filechain.datatypes.Hash256;
import filechain.primitives.Block;
import filechain.primitives.BlockHeader;
import filechain.primitives.OutPoint;
import filechain.primitives.Script;
import filechain.primitives.Transaction;
import filechain.primitives.TxIn;
import filechain.primitives.TxOut;

public class Stream {

	private static final int UINT16 = 2;
	private static final int UINT32 = 4;
	private static final int UINT64 = 8;
	private static final int UINT256 = 32;

	private ByteArrayInputStream sIn = null;
	private ByteArrayOutputStream sOut = null;

	private byte[] bytes = null;

	public Stream() {
		sOut = new ByteArrayOutputStream();
	}

	public Stream(byte[] bytesIn) {
		bytes = bytesIn;
		sIn = new ByteArrayInputStream(bytes);
	}

	// Read BYTES
	private byte[] readBytes(int size) {
		byte[] bytes = new byte[size];
		sIn.readNBytes(bytes, 0, size);
		byte[] rev = new byte[size];
		for (int i = 0; i < size; i++)
			rev[size - 1 - i] = bytes[i];
		return rev;
	}

	// Read UINT16
	private Short read16() throws IOException {
		bytes = readBytes(UINT16);
		short value = ByteBuffer.wrap(bytes).getShort();
		return value;
	}

	// Read UINT32
	private Integer read32() throws IOException {
		bytes = readBytes(UINT32);
		int value = ByteBuffer.wrap(bytes).getInt();
		return value;
	}

	// Read UINT64
	private Long read64() throws IOException {
		bytes = readBytes(UINT64);
		long value = ByteBuffer.wrap(bytes).getLong();
		return value;
	}

	// Read UINT256
	private Hash256 read256() throws IOException {
		Hash256 hash = new Hash256();
		bytes = readBytes(UINT256);
		hash.set(bytes);
		return hash;
	}

	// Read BLOCKHEADER
	private BlockHeader readBlockHeader() throws IOException {
		BlockHeader header = new BlockHeader();
		header.unserialize(this);
		return header;
	}

	// Read BLOCK
	private Block readBlock() throws IOException {
		Block block = new Block();
		block.unserialize(this);
		return block;
	}

	// Read TRANSACTION
	private Transaction readTransaction() throws IOException {
		Transaction tx = new Transaction();
		tx.unserialize(this);
		return tx;
	}

	// Read TXIN
	private TxIn readTxIn() throws IOException {
		TxIn txin = new TxIn();
		txin.unserialize(this);
		return txin;
	}

	// Read TXOUT
	private TxOut readTxOut() throws IOException {
		TxOut txout = new TxOut();
		txout.unserialize(this);
		return txout;
	}

	// Read SCRIPT
	private Script readScript() throws IOException {
		Script script = new Script();
		script.unserialize(this);
		return script;
	}

	// Read OUTPOINT
	private OutPoint readOutPoint() throws IOException {
		OutPoint outpoint = new OutPoint();
		outpoint.unserialize(this);
		return outpoint;
	}

	// Write BYTES
	private void writeBytes(byte[] bytes) {
		int size = bytes.length;
		byte[] rev = new byte[size];
		for (int i = 0; i < size; i++)
			rev[size - 1 - i] = bytes[i];
		sOut.write(rev, 0, size);
	}

	// Write UINT16
	private void write16(short value) throws IOException {
		int n = Short.toUnsignedInt(value);
		if (n < 0 || n > 65535)
			throw new IOException("value out of range: " + n);
		bytes = ByteBuffer.allocate(2).putShort(value).array();
		writeBytes(bytes);
	}

	// Write UINT32
	private void write32(int value) throws IOException {
		long n = Integer.toUnsignedLong(value);
		if (n < 0L || n > 4294967295L)
			throw new IOException("value out of range: " + n);
		bytes = ByteBuffer.allocate(4).putInt(value).array();
		writeBytes(bytes);
	}

	// Write UINT64
	private void write64(long value) throws IOException {
		BigInteger MIN = new BigInteger("0");
		BigInteger MAX = new BigInteger("18446744073709551615");

		BigInteger n = new BigInteger(Long.toUnsignedString(value));
		if (n.compareTo(MIN) < 0 || n.compareTo(MAX) > 0)
			throw new IOException("value out of range: " + n);
		bytes = ByteBuffer.allocate(8).putLong(value).array();
		writeBytes(bytes);
	}

	// Write UINT256
	private void write256(Hash256 hash) throws IOException {
		if (hash.data().length != 32)
			throw new IOException("value out of range: " + hash.data().length + " != 32");
		bytes = hash.data();
		writeBytes(bytes);
	}

	// Write BLOCKHEADER
	private void writeBlockHeader(BlockHeader header) throws IOException {
		header.serialize(this);
	}

	// Write BLOCK
	private void writeBlock(Block block) throws IOException {
		block.serialize(this);
	}

	// Write TRANSACTION
	private void writeTransaction(Transaction tx) throws IOException {
		tx.serialize(this);
	}

	// Write TXIN
	private void writeTxIn(TxIn txin) throws IOException {
		txin.serialize(this);
	}

	// Write TXOUT
	private void writeTxOut(TxOut txout) throws IOException {
		txout.serialize(this);
	}

	// Write SCRIPT
	private void writeScript(Script script) throws IOException {
		script.serialize(this);
	}

	// Write OUTPOINT
	private void writeOutPoint(OutPoint outpoint) throws IOException {
		outpoint.serialize(this);
	}

	// Read
	@SuppressWarnings("unchecked")
	public <C, T> C read(T obj) throws IOException {
		if (obj.getClass() == Byte[].class) {
			return (C) readBytes(((byte[]) obj).length);
		} else if (obj.getClass() == Short.class) {
			return (C) read16();
		} else if (obj.getClass() == Integer.class) {
			return (C) read32();
		} else if (obj.getClass() == Long.class) {
			return (C) read64();
		} else if (obj.getClass() == Hash256.class) {
			return (C) read256();
		} else if (obj.getClass() == BlockHeader.class) {
			return (C) readBlockHeader();
		} else if (obj.getClass() == Block.class) {
			return (C) readBlock();
		} else if (obj.getClass() == Transaction.class) {
			return (C) readTransaction();
		} else if (obj.getClass() == TxIn.class) {
			return (C) readTxIn();
		} else if (obj.getClass() == TxOut.class) {
			return (C) readTxOut();
		} else if (obj.getClass() == Script.class) {
			return (C) readScript();
		} else if (obj.getClass() == OutPoint.class) {
			return (C) readOutPoint();
		} else {
			throw new IOException("Invalid class: " + obj.getClass());
		}
	}

	// Write
	public <T> void write(T obj) throws IOException {
		if (obj.getClass() == Byte[].class) {
			writeBytes((byte[]) obj);
		} else if (obj.getClass() == Short.class) {
			write16((short) obj);
		} else if (obj.getClass() == Integer.class) {
			write32((int) obj);
		} else if (obj.getClass() == Long.class) {
			write64((long) obj);
		} else if (obj.getClass() == Hash256.class) {
			write256((Hash256) obj);
		} else if (obj.getClass() == BlockHeader.class) {
			writeBlockHeader((BlockHeader) obj);
		} else if (obj.getClass() == Block.class) {
			writeBlock((Block) obj);
		} else if (obj.getClass() == Transaction.class) {
			writeTransaction((Transaction) obj);
		} else if (obj.getClass() == TxIn.class) {
			writeTxIn((TxIn) obj);
		} else if (obj.getClass() == TxOut.class) {
			writeTxOut((TxOut) obj);
		} else if (obj.getClass() == Script.class) {
			writeScript((Script) obj);
		} else if (obj.getClass() == OutPoint.class) {
			writeOutPoint((OutPoint) obj);
		} else {
			throw new IOException("Invalid class: " + obj.getClass());
		}
	}

	// Read ARRAY
	@SuppressWarnings("unchecked")
	public <C, T> ArrayList<C> readArray(T obj) throws IOException {
		short size = 0;
		size = read16();

		ArrayList<C> arr = new ArrayList<C>();

		for (int i = 0; i < size; i++) {
			if (obj == Transaction.class)
				arr.add((C) read(new Transaction()));
			if (obj == TxIn.class)
				arr.add((C) read(new TxIn()));
			if (obj == TxOut.class)
				arr.add((C) read(new TxOut()));
		}

		return arr;
	}

	// Write ARRAY
	public <C> void writeArray(ArrayList<C> arr) throws IOException {
		write16((short) arr.size());
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).getClass() == Transaction.class)
				write((Transaction) arr.get(i));
			if (arr.get(i).getClass() == TxIn.class)
				write((TxIn) arr.get(i));
			if (arr.get(i).getClass() == TxOut.class)
				write((TxOut) arr.get(i));
		}
	}

	public int size() {
		if (bytes == null)
			return 0;
		return bytes.length;
	}

	public byte[] data() {
		return data(false);
	}

	public byte[] data(boolean reverse) {
		if (reverse) {
			byte[] bytes = sOut.toByteArray();
			int size = bytes.length;
			byte[] rev = new byte[size];
			for (int i = 0; i < size; i++)
				rev[size - 1 - i] = bytes[i];
			return rev;
		} else {
			return sOut.toByteArray();
		}
	}

}
