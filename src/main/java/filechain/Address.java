/**
 * Copyright (c) 2022 Marco Fortina
 * Distributed under the MIT software license, see the accompanying
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

package filechain;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Address {

	private PrivateKey privateKey = null;
	private PublicKey publicKey = null;

	public Address() {
		setNull();
	}

	public Address(PrivateKey privateKeyIn, PublicKey publicKeyIn) {
		privateKey = privateKeyIn;
		publicKey = publicKeyIn;
	}

	public void setNull() {
		privateKey = null;
		publicKey = null;
	}

	public void newKey() {
		KeyPairGenerator generator;

		try {
			generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(2048);
			KeyPair pair = generator.generateKeyPair();

			privateKey = pair.getPrivate();
			publicKey = pair.getPublic();
		} catch (NoSuchAlgorithmException e) {
			// This should never happen
			e.printStackTrace();
		}
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public byte[] getPrivateEncoded() {
		return privateKey.getEncoded();
	}

	public byte[] getPublicEncoded() {
		return publicKey.getEncoded();
	}

}
